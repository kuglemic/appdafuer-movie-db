# Appdafuer Movie Database

This is a little project to test out new technology! It uses [The Movie Database API](https://developers.themoviedb.org/3/movies).

## Technology
The project is set up using [Ignite](https://github.com/infinitered/ignite). A React-Native CLI to generate a Boilerplate project and screens, somponents and MST models. We are using the Bowser Template which uses the following technology:
* React Navigation 5
* MobX State Tree
* TypeScript
* Storybook

To request data from the API we used [react-query](https://github.com/tannerlinsley/react-query). See `app/services/index.ts`. The other files in the `app/service` folders are coming from the boilerplate project and are not used in the moment.

As UI Library we used [nativebase.io](https://nativebase.io/).

## Develop
To start developing clone the repo and run:
```
npm install
react-native run-ios
```

## Storybook
If you want to see the components in the Storybook just change this line in index.js:
```
const SHOW_STORYBOOK = true
```

## Screenshots
![](screenshots/categories.png)
![](screenshots/movieList.png)
![](screenshots/movieDetail.png)
![](screenshots/watchlist.png)


## Future Work
This is a demo project! If you want to use it in production you should 
* add proper error handling
* add loading indicators
* invest some more love to styling
* add some more features
* add some Tests
* add an app icon
* add a splash screen
* test the android build
* ...
