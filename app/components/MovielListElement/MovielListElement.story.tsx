import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { MovielListElement } from "./MovielListElement"

declare var module

storiesOf("MovielListElement", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <MovielListElement
          movie={{
            id: 1,
            title: "Movie Title",
            overview: "The description of the movie",
            vote_average: 9.3,
            release_date: "2020-07-27",
            poster_path: "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
          }}
          onPress={() => {}}
        />
      </UseCase>
    </Story>
  ))
