import React, { FunctionComponent as Component } from "react"
import { useObserver } from "mobx-react-lite"
import { ListItem, Thumbnail, Text, Left, Body, Right, Icon } from "native-base"
import { createImageUrl } from "../../services"
import { WatchlistElement, useStores } from "../../models"

export interface MovielListElementProps {
  movie: WatchlistElement
  onPress: () => void
}

export const MovielListElement: Component<MovielListElementProps> = props => {
  const movie = props.movie

  const { watchlist } = useStores()
  const isOnWatchlist = watchlist.elements.findIndex(element => element.id === movie.id) !== -1

  return useObserver(() => (
    <ListItem thumbnail onPress={props.onPress}>
      <Left>
        <Thumbnail square source={{ uri: createImageUrl(movie.poster_path, "w92") }} />
      </Left>
      <Body>
        <Text>
          {isOnWatchlist ? (
            <>
              {`${movie.title} `}
              <Icon type="MaterialIcons" name="favorite" style={{ fontSize: 12 }} />
            </>
          ) : (
            movie.title
          )}
        </Text>

        <Text note numberOfLines={1}>
          {movie.overview}
        </Text>
      </Body>
      <Right>
        <Icon type="MaterialIcons" name="chevron-right" />
      </Right>
    </ListItem>
  ))
}
