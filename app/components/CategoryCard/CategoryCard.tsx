import React, { FunctionComponent as Component } from "react"

import { ListItem, Text, Body, Right, Icon } from "native-base"

export interface CategoryCardProps {
  name: string
  onPress: () => void
}

export const CategoryCard: Component<CategoryCardProps> = props => {
  return (
    <ListItem thumbnail onPress={props.onPress}>
      <Body>
        <Text>{props.name}</Text>
      </Body>
      <Right>
        <Icon type="MaterialIcons" name="chevron-right" />
      </Right>
    </ListItem>
  )
}
