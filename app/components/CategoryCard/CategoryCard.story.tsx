import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { CategoryCard } from "./CategoryCard"

declare var module

storiesOf("CategoryCard", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <CategoryCard name="Category Name" onPress={() => {}} />
      </UseCase>
    </Story>
  ))
