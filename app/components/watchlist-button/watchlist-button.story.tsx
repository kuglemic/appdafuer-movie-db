import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { WatchlistButton } from "./watchlist-button"

declare var module

storiesOf("WatchlistButton", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="On Watchlist" usage="If the Movie is already on the watchlist">
        <WatchlistButton isOnWatchlist={true} onPress={() => {}} />
      </UseCase>
      <UseCase text="Not on Watchlist" usage="If the Movie is not yet on the watchlist">
        <WatchlistButton isOnWatchlist={false} onPress={() => {}} />
      </UseCase>
    </Story>
  ))
