import * as React from "react"
import { watchlistButtonStyles as styles } from "./watchlist-button.styles"
import { Button, Icon, Text } from "native-base"
import { translate } from "../../i18n"

export interface WatchlistButtonProps {
  isOnWatchlist: boolean
  onPress: () => void
}

export function WatchlistButton(props: WatchlistButtonProps) {
  const { isOnWatchlist, onPress, ...rest } = props

  const watchlistIcon = isOnWatchlist ? "favorite-border" : "favorite"
  const watchlistText = isOnWatchlist
    ? translate("watchlistButton.remove")
    : translate("watchlistButton.add")

  return (
    <Button style={styles.BUTTON} onPress={onPress} {...rest}>
      <Icon type="MaterialIcons" name={watchlistIcon} />
      <Text style={styles.TEXT}>{watchlistText}</Text>
    </Button>
  )
}
