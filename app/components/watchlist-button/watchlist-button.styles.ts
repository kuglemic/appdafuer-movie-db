import { spacing } from "../../theme"
import { ViewStyle, TextStyle } from "react-native"

export const watchlistButtonStyles = {
  BUTTON: {
    justifyContent: "center",
    alignSelf: "center",
    margin: spacing[3],
    width: 300,
  } as ViewStyle,
  TEXT: { paddingLeft: spacing[0] } as TextStyle,
}
