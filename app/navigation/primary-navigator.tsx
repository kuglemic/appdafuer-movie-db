/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"

import { createNativeStackNavigator } from "react-native-screens/native-stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { CategoryScreen, MovieDetailsScreen, MoviesScreen, WatchlistScreen } from "../screens"
import { Icon } from "native-base"
import { WatchlistElement } from "../models"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */

type MovieDetailProps = {
  movie: WatchlistElement
  tab: "watchlist" | "movies"
}

export type MoviesParamList = {
  category: undefined
  movies: {
    genreId: number
    genreName: string
  }
  movieDetail: MovieDetailProps
}

export type WatchlistParamList = {
  watchlist: undefined
  movieDetail: MovieDetailProps
}

// Documentation: https://github.com/software-mansion/react-native-screens/tree/master/native-stack
const MoviesStack = createNativeStackNavigator<MoviesParamList>()
const WatchlistStack = createNativeStackNavigator<WatchlistParamList>()

const Tab = createBottomTabNavigator()

export function MoviesStackNavigator() {
  return (
    <MoviesStack.Navigator
      screenOptions={{
        gestureEnabled: true,
      }}
    >
      <MoviesStack.Screen
        name="category"
        component={CategoryScreen}
        options={{ headerLargeTitle: true, title: "Categories" }}
      />
      <MoviesStack.Screen
        name="movies"
        component={MoviesScreen}
        options={navigation => ({
          headerLargeTitle: true,
          title: navigation.route.params.genreName,
        })}
      />
      <MoviesStack.Screen
        name="movieDetail"
        component={MovieDetailsScreen}
        options={navigation => ({
          headerLargeTitle: true,
          title: navigation.route.params.movie.title,
        })}
      />
    </MoviesStack.Navigator>
  )
}

export function WatchlistStackNavigator() {
  return (
    <WatchlistStack.Navigator
      screenOptions={{
        gestureEnabled: true,
      }}
    >
      <WatchlistStack.Screen
        name="watchlist"
        component={WatchlistScreen}
        options={{ headerLargeTitle: true, title: "Watchlist" }}
      />
      <WatchlistStack.Screen
        name="movieDetail"
        component={MovieDetailsScreen}
        options={navigation => ({
          headerLargeTitle: true,
          title: navigation.route.params.movie.title,
        })}
      />
    </WatchlistStack.Navigator>
  )
}

export function PrimaryNavigator() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: "blue",
        inactiveTintColor: "gray",
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName
          if (route.name === "MoviesStack") {
            iconName = "local-movies"
          } else if (route.name === "WatchlistStack") {
            iconName = "favorite"
          }
          return <Icon type="MaterialIcons" name={iconName} style={{ color, fontSize: size }} />
        },
      })}
    >
      <Tab.Screen
        options={{
          title: "Movies",
        }}
        name="MoviesStack"
        component={MoviesStackNavigator}
      />
      <Tab.Screen
        options={{
          title: "Watchlist",
        }}
        name="WatchlistStack"
        component={WatchlistStackNavigator}
      />
    </Tab.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["category"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
