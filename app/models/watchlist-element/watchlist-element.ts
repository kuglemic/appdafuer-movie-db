import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { Movie } from "../../services"

/**
 * Model description here for TypeScript hints.
 */
export const WatchlistElementModel = types
  .model("WatchlistElement")
  .props({
    id: types.number,
    poster_path: types.string,
    title: types.string,
    vote_average: types.number,
    overview: types.string,
    release_date: types.string,
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type WatchlistElementType = Instance<typeof WatchlistElementModel>
export interface WatchlistElement extends WatchlistElementType {}
type WatchlistElementSnapshotType = SnapshotOut<typeof WatchlistElementModel>
export interface WatchlistElementSnapshot extends WatchlistElementSnapshotType {}
