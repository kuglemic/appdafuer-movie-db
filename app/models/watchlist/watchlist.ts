import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { WatchlistElementModel, WatchlistElement } from "../watchlist-element/watchlist-element"

/**
 * Model description here for TypeScript hints.
 */
export const WatchlistModel = types
  .model("Watchlist")
  .props({
    elements: types.array(WatchlistElementModel),
  })
  .views(self => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions(self => ({
    addOrRemoveWatchlistElement(movie: WatchlistElement) {
      const index = self.elements.findIndex(element => element.id === movie.id)
      if (index === -1) {
        self.elements.push(movie)
      } else {
        self.elements.splice(index, 1)
      }
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type WatchlistType = Instance<typeof WatchlistModel>
export interface Watchlist extends WatchlistType {}
type WatchlistSnapshotType = SnapshotOut<typeof WatchlistModel>
export interface WatchlistSnapshot extends WatchlistSnapshotType {}
