import React, { FunctionComponent as Component } from "react"
import { observer } from "mobx-react-lite"
import { CategoryCard } from "../components"
import { useNavigation } from "@react-navigation/native"

import { Container, Content, Text } from "native-base"
import { useCategories } from "../services"

export const CategoryScreen: Component = observer(function CategoryScreen() {
  const navigation = useNavigation()

  const { isLoading, error, data: categories } = useCategories()

  if (isLoading) return <Text>Loading...</Text>

  if (error) return <Text>{`An error has occurred: ${error.message}`}</Text>

  return (
    <Container>
      <Content>
        {categories.genres.map(genre => (
          <CategoryCard
            key={genre.id}
            name={genre.name}
            onPress={() => {
              navigation.navigate("movies", { genreId: genre.id, genreName: genre.name })
            }}
          />
        ))}
      </Content>
    </Container>
  )
})
