import React, { FunctionComponent as Component, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ListRenderItemInfo, FlatList } from "react-native"
import { MovielListElement } from "../components"
import { RouteProp, useNavigation } from "@react-navigation/native"

import { MoviesParamList } from "../navigation"
import { Movie, useMoviesInfinite } from "../services"
import { List, Text } from "native-base"

type Props = {
  route: RouteProp<MoviesParamList, "movies">
}

export const MoviesScreen: Component = observer(function MoviesScreen(props: Props) {
  const genreId = props.route.params.genreId
  const navigation = useNavigation()

  const { data: movieChunks, fetchMore, canFetchMore, error } = useMoviesInfinite(genreId)

  const [movies, setMovies] = useState<Movie[]>([])
  const [numberOfChunks, setNumberOfChunks] = useState<number>(0)

  if (error) return <Text>{`An error has occurred: ${error.message}`}</Text>

  if (movieChunks && movieChunks.length > numberOfChunks) {
    // if we have more chunks loaded than in local state, append them to the state
    let newMovies: Movie[] = movies
    for (let i = numberOfChunks; i < movieChunks.length; i++) {
      newMovies = [...newMovies, ...movieChunks[i].results]
    }
    setMovies(newMovies)
    setNumberOfChunks(movieChunks.length)
  }

  const renderItem = (itemInfo: ListRenderItemInfo<Movie>) => {
    return (
      <MovielListElement
        movie={itemInfo.item}
        onPress={() => {
          navigation.navigate("movieDetail", { movie: itemInfo.item, tab: "movies" })
        }}
      />
    )
  }

  return (
    <View>
      <List>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={movie => movie.id.toString()}
          data={movies}
          renderItem={renderItem}
          onEndReached={() => {
            if (canFetchMore) {
              fetchMore()
            }
          }}
        ></FlatList>
      </List>
    </View>
  )
})
