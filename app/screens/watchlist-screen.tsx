import React, { FunctionComponent as Component } from "react"
import { observer } from "mobx-react-lite"
import { MovielListElement } from "../components"
import { useStores } from "../models"
import { Container, Content, List } from "native-base"
import { useNavigation } from "@react-navigation/native"

export const WatchlistScreen: Component = observer(function WatchlistScreen() {
  const { watchlist } = useStores()
  const navigation = useNavigation()
  return (
    <Container>
      <Content>
        <List>
          {watchlist.elements.map(movie => (
            <MovielListElement
              movie={movie}
              key={movie.id.toString()}
              onPress={() => {
                navigation.navigate("movieDetail", { movie, tab: "watchlist" })
              }}
            />
          ))}
        </List>
      </Content>
    </Container>
  )
})
