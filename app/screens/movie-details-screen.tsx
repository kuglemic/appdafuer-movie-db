import React, { FunctionComponent as Component } from "react"
import { observer } from "mobx-react-lite"
import { Image } from "react-native"
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
} from "native-base"

import { useNavigation } from "@react-navigation/native"
import { useStores } from "../models"
import { RouteProp } from "@react-navigation/native"
import { MoviesParamList } from "../navigation"
import { createImageUrl } from "../services"
import { WatchlistButton } from "../components/watchlist-button/watchlist-button"

type Props = {
  route: RouteProp<MoviesParamList, "movieDetail">
}

export const MovieDetailsScreen: Component = observer(function MovieDetailsScreen(props: Props) {
  const movie = props.route.params.movie
  const goBackOnRemove = props.route.params.tab === "watchlist"
  const navigation = useNavigation()
  const { watchlist } = useStores()

  const isOnWatchlist = watchlist.elements.findIndex(element => element.id === movie.id) !== -1

  return (
    <Container>
      <Content>
        <Card>
          <CardItem cardBody>
            <Image
              source={{
                uri: createImageUrl(movie.poster_path),
              }}
              style={{ aspectRatio: 780 / 780, width: null, flex: 1 }}
            />
          </CardItem>
          <CardItem style={{ flexDirection: "column" }}>
            <Body>
              <Text>{movie.overview}</Text>
            </Body>

            <WatchlistButton
              isOnWatchlist={isOnWatchlist}
              onPress={() => {
                if (isOnWatchlist && goBackOnRemove) {
                  navigation.goBack()
                }
                watchlist.addOrRemoveWatchlistElement(movie)
              }}
            />
          </CardItem>
          <CardItem footer>
            <Left>
              <Button transparent disabled={true}>
                <Icon type="AntDesign" name="like2" />
                <Text>{movie.vote_average}</Text>
              </Button>
            </Left>

            <Right>
              <Button transparent disabled={true}>
                <Icon type="AntDesign" name="clockcircleo" />
                <Text>{movie.release_date}</Text>
              </Button>
            </Right>
          </CardItem>
        </Card>
      </Content>
    </Container>
  )
})
