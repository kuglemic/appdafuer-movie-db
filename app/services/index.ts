import { useQuery, useInfiniteQuery } from "react-query"
import { API_URL, API_KEY } from "../config/env"

type Genre = {
  id: number
  name: string
}

type Categories = {
  genres: Genre[]
}

type MoviesChunk = {
  page: number
  total_results: number
  total_pages: number
  results: Movie[]
}

export type Movie = {
  popularity: number
  vote_count: number
  video: boolean
  poster_path: string
  id: number
  adult: boolean
  backdrop_path: string
  original_language: string
  original_title: string
  genre_ids: number[]
  title: string
  vote_average: number
  overview: string
  release_date: string
}

export const createImageUrl = (path: string, size: "w92" | "w780" = "w780") => {
  const url = `https://image.tmdb.org/t/p/${size}${path}`
  return url
}

const createApiUrl = (path: string, params: { [name: string]: string } = {}) => {
  let url = `${API_URL}${path}?api_key=${API_KEY}`
  for (const paramName in params) {
    const paramValue = params[paramName]
    url += `&${paramName}=${paramValue}`
  }
  return url
}

export const useCategories = () => {
  const result = useQuery<Categories, "fetchCategories">("fetchCategories", () => {
    const url = createApiUrl("/genre/movie/list")
    return fetch(url).then(res => res.json())
  })
  return result
}

export const useMoviesInfinite = (genreId: number) => {
  const result = useInfiniteQuery<MoviesChunk, string, number>(
    `genre${genreId}`,
    (key, page = 1) => {
      const url = createApiUrl("/discover/movie", {
        with_genres: genreId.toString(),
        page: page.toString(),
      })
      return fetch(url).then(res => res.json())
    },
    { getFetchMore: lastPage => lastPage.page + 1 },
  )
  return result
}
